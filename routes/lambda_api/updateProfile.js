const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({region: 'us-east-1', apiVersion: '2012-08-10'});

const DynamoDB = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});
const tableName = "UserProfile";

exports.updateProfile = (event, context, callback) => {

// var ppl = event.ProfilePictureUrl;
var UserId = event.UserId;
//var favorites = event.FavoritePlaces;
var updateExp = "set ";
var reqBodySize = Object.keys(event).length; //number of elements in the request body
console.log(reqBodySize);

var expAttr = {};

if (event.ProfilePictureUrl != null && event.ProfilePictureUrl != ''){
    var ppl = event.ProfilePictureUrl;
    updateExp += 'ProfilePictureUrl = :p,';
    expAttr[':p'] = ppl;
}
if ( event.Username != null && event.Username != ''){
    var username = event.Username;
    expAttr[':u'] = username;
    updateExp += 'Username = :u,';
}
if (event.FavoritePlaces != null){
    updateExp += 'FavoritesPlaces = :f,';
    var favorites = event.FavoritePlaces;
    expAttr[':f'] = favorites;
}
updateExp = updateExp.substring(0, updateExp.length - 1);

// if (reqBodySize == 2){
//     if (ppl != null && ppl != ''){
//         updateExp += 'ProfilePictureUrl = :p';
//         expAttr = {
//             ":p" : ppl
//         };
//     }
//     if (favorites != null){
//         updateExp += 'FavoritesPlaces = :f';
//         expAttr = {
//             ":f" : favorites
//         };
//     }
   
// }
// if (reqBodySize == 3){
//     updateExp += 'ProfilePictureUrl = :p, FavoritesPlaces = :f';
//     expAttr = {
//         ":p" : ppl,
//         ":f" : favorites
//     };
// }

console.log('update expression');
console.log(updateExp);
console.log('expression attribute');
console.log(expAttr);

// Update the item, unconditionally,
var params = {
    TableName: tableName,
    Key: {
        "UserId" : UserId
    },
    UpdateExpression: updateExp,
    ExpressionAttributeValues: expAttr,
    ReturnValues:"UPDATED_NEW"
};

console.log("Updating the item...");
DynamoDB.update(params, function(err, data) {
    if (err) {
        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        var responseObj = {
            statusCode : 201,
            message : err
        };
        callback(null, responseObj);
    } else {
        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        var responseObj = {
            statusCode : 200,
            message : "update successfully"
        };
        callback(null, responseObj);
    }
});

};
