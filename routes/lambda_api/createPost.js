const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({region: 'us-east-1', apiVersion: '2012-08-10'});

exports.fn = (event, context, callback) => {
    console.log(event);
    const params = {
        Item: {
            "UserId": {
                S: event.UserId
            },
            "EventID": {
                S: event.EventID
            },
            "Name": {
                S: event.Name
            },
            "Detail": {
                S: event.Detail
            },
            "Place": {
                S: event.Place
            },
            "Address": {
                S: event.Address
            },
            "Latitude": {
                N: event.Latitude
            },
            "Longitude": {
                N: event.Longitude
            },
            "Zipcode": {
                N: event.Zipcode
            },
            "EventDate": {
                S: event.EventDate
            },
            "CreateDate": {
                S: event.CreateDate
            },
        },
        TableName: "Event"
    };
    dynamodb.putItem(params, function(err, data) {
        if (err) {
            console.log(err);
            callback(err);
        } else {
            console.log(data);
            callback(null, data);
        }
    });
};
