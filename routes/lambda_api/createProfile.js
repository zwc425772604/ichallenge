const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({region: 'us-east-1', apiVersion: '2012-08-10'});
//const express = require('express');

const DynamoDB = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});


exports.createProfile = (event, context, callback) => {
    var favorites = event.FavoritePlaces;
    // console.log(favorites);
    // console.log(event.body.UserId);
    // console.log(event.body.ProfilePictureUrl);
    var params = {
        TableName: "UserProfile",
        Item: {
            "UserId": event.UserId,
            "ProfilePictureUrl" : event.ProfilePictureUrl,
            "FavoritesPlaces": favorites
        } 
    };
    DynamoDB.put(params, function (err,data) {
        if (err) {
            console.log(err);
            callback(err);
            
        }else{
            console.log(data);
            //callback(null, data);
            var responseObj = {
                statusCode : 200,
                message : "success"
            };
            callback(null, responseObj);
        }

   });
};