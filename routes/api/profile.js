const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({region: 'us-east-1', apiVersion: '2012-08-10'});
const express = require('express');
const randomstring = require("randomstring");
const DynamoDB = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});
const tableName = "UserProfile";
const path = require('path');
const S3 = new AWS.S3({
    accessKeyId: 'AKIA5SOQBG475OFA7UP4',
    secretAccessKey: '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'
  });

exports.createProfile = (event, context, callback) => {
    // var favorites = [{"name": "Pocha 32", "address" : "32 w 32nd st Manhattan"}, {"name" : "Vivi Bubble Tea", "address" :"8602 86th st Brooklyn NY 11214"}]
    var favorites = event.body.FavoritePlaces;
    // console.log(favorites);
    // console.log(event.body.UserId);
    // console.log(event.body.ProfilePictureUrl);
    var params = {
        TableName: tableName,
        Item: {
            "UserId": event.body.UserId,
            "ProfilePictureUrl" : event.body.ProfilePictureUrl,
            "FavoritesPlaces": favorites
        } 
    };
    DynamoDB.put(params, function (err,data) {
        if (err) {
            //console.log(err);
            callback(err);
            context.json({
                "message" : "failure to create the profile",
                "error" : err
            })
        }else{
            console.log(data);
            //callback(null, data);
            context.json({
                "message" : "success",
                "data" : data,
                "FavoritePlace" : favorites
            })
        }

   });
};

exports.updateProfile = (event, context, callback) => {
console.log(event);
//var ppl = event.body.ProfilePictureUrl;
var UserId = event.body.UserId;
// var favorites = event.body.FavoritePlaces;
// var username = event.body.Username;
var updateExp = "set ";
var expAttr;
var reqBodySize = Object.keys(event.body).length; //number of elements in the request body
console.log(reqBodySize);

var expAttr = {};

if (event.body.ProfilePictureUrl != null && event.body.ProfilePictureUrl != ''){
    var ppl = event.body.ProfilePictureUrl;
    updateExp += 'ProfilePictureUrl = :p,';
    expAttr[':p'] = ppl;
}
if ( event.body.Username != null && event.body.Username != ''){
    var username = event.body.Username;
    expAttr[':u'] = username;
    updateExp += 'Username = :u,';
}
if (event.body.FavoritePlaces != null){
    updateExp += 'FavoritesPlaces = :f,';
    var favorites = event.body.FavoritePlaces;
    expAttr[':f'] = favorites;
}
updateExp = updateExp.substring(0, updateExp.length - 1);

// if (reqBodySize == 2){
//     if (ppl != null && ppl != ''){
//         updateExp += 'ProfilePictureUrl = :p';
//         expAttr = {
//             ":p" : ppl
//         };
//     }
//     if (favorites != null){
//         updateExp += 'FavoritesPlaces = :f';
//         expAttr = {
//             ":f" : favorites
//         };
//     }
   
// }
// if (reqBodySize == 3){
//     updateExp += 'ProfilePictureUrl = :p, FavoritesPlaces = :f';
//     expAttr = {
//         ":p" : ppl,
//         ":f" : favorites
//     };
// }
console.log('update expression');
console.log(updateExp);
console.log('expression attribute');
console.log(expAttr);


// Update the item, unconditionally,
var params = {
    TableName: tableName,
    Key: {
        "UserId" : UserId
    },
    UpdateExpression: updateExp,
    ExpressionAttributeValues: expAttr,
    ReturnValues:"UPDATED_NEW"
};

console.log("Updating the item...");
DynamoDB.update(params, function(err, data) {
    if (err) {
        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
        var responseObj = {
            statusCode : 200,
            message : err
        };
        callback(JSON.stringify(responseObj));
    } else {
        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
        var responseObj = {
            statusCode : 200,
            message : "update successfully"
        };
        //callback(JSON.stringify(responseObj), null);
         context.json({
            message : "update successfully"
            
         });
    }
});
// context.json({
//                 message : "update successfully"
                
//              });

};



exports.uploadProfilePicture = (event, context, callback) => {
    
    const userid = event.body.UserId;
    console.log(userid);
    
    var picture_id = randomstring.generate();
    // The file upload has completed
    const file = event.files.ProfilePicture;
    var extension = path.extname(file.name);
    console.log(file.name);
    const fileSize = file.size;


    const params = {
        Bucket: 'tagalongme', // pass your bucket name
        Key: 'profile/' + userid + '/' + picture_id + extension, //file name stored in s3
        Body: file.data
    };
    S3.upload(params, function(s3Err, data) {
        if (s3Err){
            console.log('error in callback');
            console.log(err);
            throw s3Err;
        }else{
            console.log(`File uploaded successfully at ${data.Location}`)
            // var responseObj = {
            //     message: "upload success",
            //     fileId: picture_id,
            //     filePath: 'https://tagalongme.s3.amazonaws.com/profile/' + userid + '/' + picture_id + extension,
            //     fileSize: fileSize
            // };
             //callback(JSON.stringify(responseObj), null);
            context.json({
                message: "upload success",
                fileId: picture_id,
                // filePath: 'https://tagalongme.s3.amazonaws.com/profile/' + userid + '/' + picture_id + extension,
                filePath: data.Location,
                fileSize: fileSize
             });
        }  
    });
};