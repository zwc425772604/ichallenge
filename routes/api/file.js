const Busboy = require('busboy');
const express = require('express');
const router = express.Router();
const path = require('path');
const AWS = require('aws-sdk');
const randomstring = require("randomstring");

router.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  next();
});
    router.post('/upload', function (req, res, next) {
        // This grabs the additional parameters so in this case passing in
        // "element1" with a value.
        const userid = req.body.userid;
        console.log(userid);
        var busboy = new Busboy({ headers: req.headers });
        
        var video_id = randomstring.generate();
        // The file upload has completed
        const file = req.files.element2;
        var extension = path.extname(file.name);
        console.log(file.name);
        const fileSize = file.size;
        busboy.on('finish', function() {
          console.log('Upload finished');
          
          // Begins the upload to the AWS S3
          filePath = uploadToS3(file,video_id, userid);
        });
    
        req.pipe(busboy);
        res.json({
            message: "upload success",
            fileId: video_id,
            filePath: 'https://tagalongme.s3.amazonaws.com/profile/' + userid + '/' + video_id + extension,
            fileSize: fileSize
          });
          
      });
      


      function uploadToS3(file, videoId, userid) {
        let s3bucket = new AWS.S3({
          accessKeyId: 'AKIA5SOQBG475OFA7UP4',
          secretAccessKey: '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',
          Bucket: 'tagalongme'
        });
        
        var extension = path.extname(file.name);
        s3bucket.createBucket(function () {
            var params = {
              Bucket: 'tagalongme',
              Key: 'profile/' + userid + '/' + videoId + extension, //file name stored in s3
              Body: file.data
            };
            s3bucket.upload(params, function (err, data) {
              if (err) {
                console.log('error in callback');
                console.log(err);
              }
              console.log('success');
              console.log(data);
              console.log("file upload location is ", data.Location);
              return data.Location
            });
        });
      }

      module.exports = router;