const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');

const busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');

const file= require('./routes/api/file');
const user = require('./routes/api/user');
const profile = require('./routes/api/profile');
//const question = require('./routes/api/question');

const app = express();
app.use(busboy());

//Body parser middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(busboyBodyParser());

app.get('/', (req,res) => res.send('Hello World'));
app.use('/api/file', file);
app.use('/api/user', user);
app.use('/api/createProfile', profile.createProfile);
app.use('/api/updateProfile', profile.updateProfile);
app.use('/api/uploadProfilePicture', profile.uploadProfilePicture);
//app.use('/api/question', question);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));